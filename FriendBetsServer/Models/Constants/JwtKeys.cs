namespace FriendBetsServer.Models.Constants
{
    public static class JwtKeys
    {
        public const string JwtIssuer = nameof(FriendBetsServer);
        public const string JwtAudience = "FriendsBet";
    }
}