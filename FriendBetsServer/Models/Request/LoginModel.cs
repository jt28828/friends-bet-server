using System.ComponentModel.DataAnnotations;

namespace FriendBetsServer.Models.Request
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Please enter an Email address"), EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter a Password")]
        public string Password { get; set; }
    }
}