namespace FriendBetsServer.Models.Request
{
    /// <summary>
    /// A reusable model for sending strings in JSON format
    /// </summary>
    public class StringModel
    {
        public string Value { get; set; }
    }
}