using System.ComponentModel.DataAnnotations;
using FriendBetsServer.Models.Enums;

namespace FriendBetsServer.Models.Request.Room
{
    public class UpdateRoomModel
    {
        public string? Name { get; set; }

        [Range(1, 100)]
        public short? MaxGuessesPerMember { get; set; }

        public RoomState? State { get; set; }
    }
}