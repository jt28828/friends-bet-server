using System.ComponentModel.DataAnnotations;

namespace FriendBetsServer.Models.Request.Room
{
    public class CreateRoomModel
    {
        [Required]
        public string Name { get; set; }

        [Required, Range(1, 100)]
        public short MaxGuessesPerMember { get; set; }
    }
}