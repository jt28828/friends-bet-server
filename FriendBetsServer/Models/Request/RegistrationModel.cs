using System.ComponentModel.DataAnnotations;

namespace FriendBetsServer.Models.Request
{
    public class RegistrationModel : LoginModel
    {
        [Required(ErrorMessage = "Please enter a Username")]
        public string Username { get; set; }
    }
}