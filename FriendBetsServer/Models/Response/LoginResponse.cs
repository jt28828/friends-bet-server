namespace FriendBetsServer.Models.Response
{
    public class LoginResponse
    {
        public string Name { get; set; }
        public string Token { get; set; }

        public LoginResponse()
        {
        }

        public LoginResponse(string name, string token)
        {
            Name = name;
            Token = token;
        }
    }
}