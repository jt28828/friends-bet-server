using FriendBetsServer.Models.Enums;

namespace FriendBetsServer.Models.Response
{
    public class RoomResponse
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public RoomState CurrentState { get; set; }

        public short MaxGuessesPerMember { get; set; }
    }
}