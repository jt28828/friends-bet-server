namespace FriendBetsServer.Models.Enums
{
    public enum RoomState
    {
        Open,
        Playing,
        Finished
    }
}