using FriendBetsServer.Database.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FriendBetsServer.Database
{
    public class FriendsDbContext : IdentityDbContext<AppUser>
    {
        public DbSet<Room> Rooms { get; set; }
        public DbSet<RoomMember> RoomMembers { get; set; }

        public FriendsDbContext(DbContextOptions<FriendsDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<RoomMember>(b =>
            {
                b.HasKey(roomMember => new {roomMember.RoomId, roomMember.MemberId});
            });

            base.OnModelCreating(builder);
        }
    }
}