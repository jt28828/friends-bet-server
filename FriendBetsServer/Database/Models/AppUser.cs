using System;
using FriendBetsServer.Models.Request;
using Microsoft.AspNetCore.Identity;

namespace FriendBetsServer.Database.Models
{
    public sealed class AppUser : IdentityUser
    {
        public string Name { get; set; }
        public AppUser()
        {

        }

        public AppUser(RegistrationModel registrationModel)
        {
            Name = registrationModel.Username;
            UserName = registrationModel.Email;
            Email = registrationModel.Email;
        }
    }
}