using System.ComponentModel.DataAnnotations.Schema;

namespace FriendBetsServer.Database.Models
{
    public class RoomMember
    {
        public string MemberId { get; set; }

        [ForeignKey(nameof(MemberId))]
        public AppUser? Member { get; set; }

        public string RoomId { get; set; }

        [ForeignKey(nameof(RoomId))]
        public Room? Room { get; set; }

        public bool MembershipApproved { get; set; }

        public RoomMember()
        {
        }

        public RoomMember(string memberId, string roomId)
        {
            MemberId = memberId;
            RoomId = roomId;
        }
    }
}