using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using FriendBetsServer.Models.Enums;

namespace FriendBetsServer.Database.Models
{
    public class Room
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Id { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// Non Hashed password because it's only used to let user into the rooms.
        /// Would actually be funny if someone broke in
        /// </summary>
        public string Password { get; set; }

        public RoomState CurrentState { get; set; }

        public short MaxGuessesPerMember { get; set; }

        public string OwnerId { get; set; }

        [ForeignKey(nameof(OwnerId))]
        public AppUser Owner { get; set; }

        public Room()
        {
            Id = Guid.NewGuid().ToString();
            Password = GenerateRoomCode();
            CurrentState = RoomState.Open;
        }

        /// <summary>
        /// Creates a Human friendly room code for people to use to spectate / join
        /// </summary>
        private string GenerateRoomCode()
        {
            var rand = new Random();
            byte[] bytes = new byte[7];
            rand.NextBytes(bytes);
            return Encoding.ASCII.GetString(bytes);
        }
    }
}