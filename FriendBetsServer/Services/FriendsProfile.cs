using AutoMapper;
using FriendBetsServer.Database.Models;
using FriendBetsServer.Models.Request;
using FriendBetsServer.Models.Request.Room;
using FriendBetsServer.Models.Response;

namespace FriendBetsServer.Services
{
    public class FriendsProfile : Profile
    {
        public FriendsProfile()
        {
            CreateMap<CreateRoomModel, Room>();
            CreateMap<Room, RoomResponse>();
        }
    }
}