using System.Security.Claims;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FriendBetsServer.Controllers.Base
{
    public abstract class FriendsController : ControllerBase
    {
        private string? _userId;

        /// <summary>
        /// Returns the Id of the currently authenticated User
        /// </summary>
        protected string? UserId => _userId ??= User.FindFirst(ClaimTypes.PrimarySid)?.Value;

        /// <summary>
        /// Returns whether or not the currently authenticated user has the same Id as provided
        /// </summary>
        protected bool MatchesUser(string id) => User.HasClaim(ClaimTypes.PrimarySid, id);

        protected ObjectResult NotFoundProblem(string? description)
        {
            return Problem(description, statusCode: StatusCodes.Status404NotFound);
        }

        protected ObjectResult BadRequestProblem(string? description)
        {
            return Problem(description, statusCode: StatusCodes.Status400BadRequest);
        }

        protected ObjectResult ConflictProblem(string? description)
        {
            return Problem(description, statusCode: StatusCodes.Status409Conflict);
        }
    }
}