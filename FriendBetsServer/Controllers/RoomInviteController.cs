using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using AutoMapper;
using FriendBetsServer.Controllers.Base;
using FriendBetsServer.Database;
using FriendBetsServer.Database.Models;
using FriendBetsServer.Models.Request;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FriendBetsServer.Controllers
{
    [Route("[controller]"), ApiController, Authorize]
    public class RoomInviteController : FriendsController
    {
        private readonly FriendsDbContext _dbContext;
        private readonly ILogger<RoomInviteController> _logger;
        private readonly IMapper _mapper;

        public RoomInviteController(FriendsDbContext dbContext, ILogger<RoomInviteController> logger, IMapper mapper)
        {
            _dbContext = dbContext;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>
        /// Sends an access request for a room to the owner
        /// </summary>
        [HttpGet("{roomCode}")]
        [ProducesResponseType(typeof(void), 204)]
        [ProducesResponseType(typeof(ProblemDetails), 404)]
        [ProducesResponseType(typeof(ProblemDetails), 500)]
        public async Task<IActionResult> RequestAccess(string roomCode)
        {
            Room? foundRoom = await _dbContext.Rooms
                .FirstOrDefaultAsync(room => room.Password == roomCode);

            if (foundRoom == null || UserId == null)
            {
                // Doesn't exist or user isn't logged in
                return NotFoundProblem("Room not found");
            }

            // Room was found, request membership
            var newMembership = new RoomMember(UserId, foundRoom.Id);

            await _dbContext.RoomMembers.AddAsync(newMembership);

            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to invite a user to a room");
                // Might have failed because they've already asked
                return Problem("Failed to request access to the room, maybe you've already requested it?");
            }

            // TODO send a message to the room owner

            return NoContent();
        }

        /// <summary>
        /// Attempts to accept an invite to a room
        /// </summary>
        [HttpPost("accept/{roomId}")]
        [ProducesResponseType(typeof(void), 204)]
        [ProducesResponseType(typeof(ProblemDetails), 404)]
        [ProducesResponseType(typeof(ProblemDetails), 500)]
        public async Task<IActionResult> AcceptRequest([FromRoute] string roomId, [FromBody] StringModel request)
        {
            Room? foundRoom = await _dbContext.Rooms
                .FindAsync(roomId);

            if (foundRoom == null || UserId == null)
            {
                // Doesn't exist or user isn't logged in
                return NotFoundProblem("Room not found");
            }

            // Room was found, find membership request
            RoomMember? membershipRequest = await _dbContext.RoomMembers
                .Where(rm => rm.RoomId == roomId)
                .FirstOrDefaultAsync(rm => rm.MemberId == request.Value);

            if (membershipRequest == null)
            {
                // Hasn't requested access
                return NotFoundProblem("That user hasn't requested access to the room yet");
            }

            // Update the request so it's accepted
            membershipRequest.MembershipApproved = true;

            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to accept a membership request");
                // Might have failed because they've already accepted
                return Problem("Failed to grant access to the room, maybe you've already accepted it?");
            }

            // TODO send a message to the person that accepted it

            return NoContent();
        }
    }
}