using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using AutoMapper;
using FriendBetsServer.Controllers.Base;
using FriendBetsServer.Database;
using FriendBetsServer.Database.Models;
using FriendBetsServer.Models.Request.Room;
using FriendBetsServer.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FriendBetsServer.Controllers
{
    [Route("[controller]"), ApiController, Authorize]
    public class RoomsController : FriendsController
    {
        private readonly FriendsDbContext _dbContext;
        private readonly ILogger<RoomsController> _logger;
        private readonly IMapper _mapper;

        public RoomsController(FriendsDbContext dbContext, ILogger<RoomsController> logger, IMapper mapper)
        {
            _dbContext = dbContext;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>
        /// Attempts to spectate a room
        /// </summary>
        [ProducesResponseType(typeof(RoomResponse), 200)]
        [ProducesResponseType(typeof(ProblemDetails), 404)]
        [HttpGet("{roomCode}"), AllowAnonymous]
        public async Task<IActionResult> SpectateJoinRoom(string roomCode)
        {
            Room? foundRoom = await _dbContext.Rooms
                .FirstOrDefaultAsync(room => room.Password == roomCode);

            if (foundRoom == null)
            {
                // Doesn't exist
                return NotFoundProblem("Room not found");
            }

            // No need to doublecheck any ownership, return room
            return Ok(_mapper.Map<RoomResponse>(foundRoom));
        }

        /// <summary>
        /// Attempts to create a new room to play in
        /// </summary>
        [ProducesResponseType(typeof(RoomResponse), 200)]
        [ProducesResponseType(typeof(ProblemDetails), 500)]
        [HttpPost]
        public async Task<IActionResult> CreateRoom(CreateRoomModel inputModel)
        {
            // See if the user already has a room
            bool hasExistingRoom = await _dbContext.Rooms
                .Where(room => room.OwnerId == UserId)
                .AnyAsync();

            if (hasExistingRoom)
            {
                return Problem("You can't be the owner of more than one room, close your existing ones first");
            }

            // Make them a room
            var newRoom = _mapper.Map<Room>(inputModel);

            await _dbContext.Rooms.AddAsync(newRoom);

            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to add a new room for a user");
                return Problem("Something went wrong creating your room");
            }

            return Ok(_mapper.Map<RoomResponse>(newRoom));
        }

        /// <summary>
        /// Attempts to update the status of an existing room
        /// </summary>
        [ProducesResponseType(typeof(RoomResponse), 200)]
        [ProducesResponseType(typeof(void), 403)]
        [ProducesResponseType(typeof(ProblemDetails), 404)]
        [ProducesResponseType(typeof(ProblemDetails), 500)]
        [HttpPut("{roomId}")]
        public async Task<IActionResult> UpdateRoom([FromRoute] string roomId, [FromBody] UpdateRoomModel inputModel)
        {
            // Get the room and confirm the current user is the admin
            Room existingRoom = await _dbContext.Rooms
                .FindAsync(roomId);

            if (existingRoom == null)
            {
                // Room doesn't exist
                return NotFoundProblem("Couldn't find a matching room to update");
            }

            if (!MatchesUser(existingRoom.OwnerId))
            {
                // User isn't the room owner
                return Forbid();
            }

            // Update the room with the values that have been actually sent
            if (!string.IsNullOrWhiteSpace(inputModel.Name))
            {
                existingRoom.Name = inputModel.Name;
            }

            if (inputModel.State != null)
            {
                existingRoom.CurrentState = inputModel.State.Value;
            }

            if (inputModel.MaxGuessesPerMember != null)
            {
                existingRoom.MaxGuessesPerMember = inputModel.MaxGuessesPerMember.Value;
            }

            // Save any updates to the db

            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to update an existing room");
                return Problem("Failed to update the room");
            }

            // Return the new state
            return Ok(_mapper.Map<RoomResponse>(existingRoom));
        }

        /// <summary>
        /// Attempts to update the status of an existing room
        /// </summary>
        [ProducesResponseType(typeof(void), 204)]
        [ProducesResponseType(typeof(void), 403)]
        [ProducesResponseType(typeof(ProblemDetails), 404)]
        [ProducesResponseType(typeof(ProblemDetails), 500)]
        [HttpDelete("{roomId}")]
        public async Task<IActionResult> DeleteRoom(string roomId)
        {
            // Get the room and confirm the current user is the admin
            Room existingRoom = await _dbContext.Rooms
                .FindAsync(roomId);

            if (existingRoom == null)
            {
                // Room doesn't exist
                return NotFoundProblem("Couldn't find a matching room to delete");
            }

            if (!MatchesUser(existingRoom.OwnerId))
            {
                // User isn't the room owner
                return Forbid();
            }

            // Destroy the room
            _dbContext.Remove(existingRoom);
            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to delete an existing room");
                return Problem("Failed to delete the room");
            }

            // Return the new state
            return NoContent();
        }
    }
}