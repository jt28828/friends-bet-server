using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using FriendBetsServer.Database.Models;
using FriendBetsServer.Models.Constants;
using FriendBetsServer.Models.Request;
using FriendBetsServer.Models.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace FriendBetsServer.Controllers
{
    [Route("[controller]"), ApiController]
    public class AccountController : ControllerBase
    {
        private readonly ILogger<AccountController> _logger;
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IConfiguration _configuration;

        public AccountController(ILogger<AccountController> logger, UserManager<AppUser> userManager,
            RoleManager<IdentityRole> roleManager, IConfiguration configuration)
        {
            _logger = logger;
            _userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;
        }

        /// <summary>
        /// Attempt to log in an existing user
        /// </summary>
        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginModel loginModel)
        {
            AppUser user = await _userManager.FindByEmailAsync(loginModel.Email);

            if (user == null)
            {
                return Problem("User with those details was not found", statusCode: StatusCodes.Status404NotFound);
            }

            if (!await _userManager.CheckPasswordAsync(user, loginModel.Password))
            {
                // Password didn't match, don't indicate there was user though
                return Problem("User with those details was not found", statusCode: StatusCodes.Status404NotFound);
            }

            // User is valid, get a JWT for them
            JwtSecurityToken newToken = await GetUserToken(user);
            string tokenText = new JwtSecurityTokenHandler().WriteToken(newToken);
            return Ok(new LoginResponse(user.Name, tokenText));
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(RegistrationModel registrationModel)
        {
            // Check if the user exists already first
            if (await _userManager.FindByEmailAsync(registrationModel.Email) != null)
            {
                return Problem("Email already in use");
            }

            // Create a new user
            var registeredUser = new AppUser(registrationModel);
            IdentityResult response = await _userManager.CreateAsync(registeredUser, registrationModel.Password);

            if (!response.Succeeded)
            {
                // Log for analysis
                string errorMessage = string.Join(
                    "\n",
                    response.Errors.Select(error => $"{error.Code}:{error.Description}")
                );
                _logger.LogError(errorMessage);

                // Return to user
                return Problem("Something went wrong creating that user, please try again");
            }

            // User was successfully created, return them a JWT
            JwtSecurityToken newToken = await GetUserToken(registeredUser);
            string tokenText = new JwtSecurityTokenHandler().WriteToken(newToken);
            return Ok(new LoginResponse(registeredUser.Name, tokenText));
        }

        /// <summary>
        /// Retrieves a token containing the credentials of the logged in user
        /// </summary>
        private async Task<JwtSecurityToken> GetUserToken(AppUser user)
        {
            // Create a JWT with a JTI to allow blacklisting.
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.PrimarySid, user.Id),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            // Get any special roles the user has too and add them
            IList<string> roles = await _userManager.GetRolesAsync(user);
            claims.AddRange(roles.Select(role => new Claim(ClaimTypes.Role, role)));

            // Finally get specific claims saved for the user as well
            IList<Claim> dbClaims = await _userManager.GetClaimsAsync(user);
            claims.AddRange(dbClaims);

            // Turn into a JWT
            string keyString = _configuration["JwtSecret"];
            var key = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(keyString)),
                SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(JwtKeys.JwtIssuer, JwtKeys.JwtAudience, claims,
                expires: DateTime.Now.AddDays(30), signingCredentials: key);

            return token;
        }
    }
}